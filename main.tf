terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
      version = "2.1.0"
    }
  }

   required_version = ">= 1.1.0"
}

provider "local" {
  # Configuration options
}

resource "local_file" "foo" {
    content     = "foo!"
    filename = "foo.bar"
}